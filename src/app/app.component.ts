import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import squarify, { Input, IRect, ILayoutRect } from 'squarify';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent implements AfterViewInit {
  rawData: string = "[{\"value\":106,\"partitions\":[{\"value\":11.0,\"name\":\"interventions sociales\"},{\"value\":18.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":16.0,\"name\":\"sant\\u00e9\"},{\"value\":16.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":8.0,\"name\":\"action socioculturelle\"},{\"value\":2.0,\"name\":\"sports\"},{\"value\":11.0,\"name\":\"amicales groupts affinit\"},{\"value\":1.0,\"name\":\"religion\"},{\"value\":1.0,\"name\":\"culture\"},{\"value\":1.0,\"name\":\"environnement\"},{\"value\":4.0,\"name\":\"services medico sociaux\"},{\"value\":1.0,\"name\":\"activit\\u00e9s \\u00e9conomiques\"},{\"value\":8.0,\"name\":\"caritatives humanit dev\"},{\"value\":1.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":1.0,\"name\":\"s\\u00e9curit\\u00e9\"},{\"value\":4.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":1.0,\"name\":\"d\\u00e9fense droits\"},{\"value\":1.0,\"name\":\"recherche\"}],\"words\":[{\"word\":\"autre\",\"score\":0.6},{\"word\":\"tout\",\"score\":0.59},{\"word\":\"concerner\",\"score\":0.58},{\"word\":\"pouvoir\",\"score\":0.58}],\"name\":\"1\"},{\"value\":98,\"partitions\":[{\"value\":87.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":3.0,\"name\":\"action socioculturelle\"},{\"value\":2.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":1.0,\"name\":\"sant\\u00e9\"},{\"value\":1.0,\"name\":\"clubs\"},{\"value\":1.0,\"name\":\"interventions sociales\"},{\"value\":2.0,\"name\":\"culture\"},{\"value\":1.0,\"name\":\"services medico sociaux\"}],\"words\":[{\"word\":\"\\u00e9l\\u00e8ve\",\"score\":0.64},{\"word\":\"\\u00e9cole\",\"score\":0.63},{\"word\":\"scolaire\",\"score\":0.6},{\"word\":\"coll\\u00e8ge\",\"score\":0.59}],\"name\":\"2\"},{\"value\":84,\"partitions\":[{\"value\":2.0,\"name\":\"clubs loisirs\"},{\"value\":49.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":14.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":3.0,\"name\":\"culture\"},{\"value\":1.0,\"name\":\"sant\\u00e9\"},{\"value\":1.0,\"name\":\"culture sciences\"},{\"value\":5.0,\"name\":\"amicales groupts affinit\"},{\"value\":1.0,\"name\":\"groupts affinitaires\"},{\"value\":1.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":3.0,\"name\":\"sports\"},{\"value\":1.0,\"name\":\"religion\"},{\"value\":1.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":1.0,\"name\":\"caritatives humanit dev\"},{\"value\":1.0,\"name\":\"services medico sociaux\"}],\"words\":[{\"word\":\"relation\",\"score\":0.6},{\"word\":\"participer\",\"score\":0.56},{\"word\":\"tout\",\"score\":0.55},{\"word\":\"contribuer\",\"score\":0.55}],\"name\":\"3\"},{\"value\":6,\"partitions\":[{\"value\":1.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":1.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":3.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":1.0,\"name\":\"\\u00e9ducation formation\"}],\"words\":[{\"word\":\"retraite\",\"score\":0.61},{\"word\":\"vieillesse\",\"score\":0.6},{\"word\":\"personne\",\"score\":0.57},{\"word\":\"invalidit\\u00e9\",\"score\":0.56}],\"name\":\"4\"},{\"value\":127,\"partitions\":[{\"value\":5.0,\"name\":\"sports\"},{\"value\":50.0,\"name\":\"culture\"},{\"value\":7.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":3.0,\"name\":\"clubs loisirs\"},{\"value\":16.0,\"name\":\"culture sciences\"},{\"value\":2.0,\"name\":\"environnement\"},{\"value\":13.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":3.0,\"name\":\"amicales groupts affinit\"},{\"value\":5.0,\"name\":\"recherche\"},{\"value\":4.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":4.0,\"name\":\"activit\\u00e9s politiques\"},{\"value\":2.0,\"name\":\"arm\\u00e9e anciens comb\"},{\"value\":1.0,\"name\":\"tourisme\"},{\"value\":3.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":2.0,\"name\":\"caritatives humanit dev\"},{\"value\":1.0,\"name\":\"religion\"},{\"value\":1.0,\"name\":\"interventions sociales\"},{\"value\":1.0,\"name\":\"sant\\u00e9\"},{\"value\":1.0,\"name\":\"aide emploi\"},{\"value\":2.0,\"name\":\"action socioculturelle\"},{\"value\":1.0,\"name\":\"info communication\"}],\"words\":[{\"word\":\"grand\",\"score\":0.62},{\"word\":\"nombreux\",\"score\":0.62},{\"word\":\"tout\",\"score\":0.61},{\"word\":\"divers\",\"score\":0.58}],\"name\":\"5\"},{\"value\":165,\"partitions\":[{\"value\":8.0,\"name\":\"amicales groupts affinit\"},{\"value\":2.0,\"name\":\"recherche\"},{\"value\":12.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":13.0,\"name\":\"interventions sociales\"},{\"value\":5.0,\"name\":\"s\\u00e9curit\\u00e9\"},{\"value\":18.0,\"name\":\"services medico sociaux\"},{\"value\":14.0,\"name\":\"caritatives humanit dev\"},{\"value\":34.0,\"name\":\"sant\\u00e9\"},{\"value\":3.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":9.0,\"name\":\"culture sciences\"},{\"value\":2.0,\"name\":\"social\"},{\"value\":5.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":2.0,\"name\":\"clubs loisirs\"},{\"value\":4.0,\"name\":\"action socioculturelle\"},{\"value\":5.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":2.0,\"name\":\"sports\"},{\"value\":5.0,\"name\":\"culture\"},{\"value\":11.0,\"name\":\"environnement\"},{\"value\":5.0,\"name\":\"d\\u00e9fense droits\"},{\"value\":1.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":2.0,\"name\":\"activit\\u00e9s politiques\"},{\"value\":1.0,\"name\":\"info communication\"},{\"value\":1.0,\"name\":\"activit\\u00e9s \\u00e9conomiques\"},{\"value\":1.0,\"name\":\"clubs\"}],\"words\":[{\"word\":\"agir\",\"score\":0.66},{\"word\":\"pouvoir\",\"score\":0.63},{\"word\":\"autre\",\"score\":0.63},{\"word\":\"tout\",\"score\":0.63}],\"name\":\"6\"},{\"value\":7,\"partitions\":[{\"value\":4.0,\"name\":\"culture sciences\"},{\"value\":1.0,\"name\":\"culture\"},{\"value\":1.0,\"name\":\"recherche\"},{\"value\":1.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"}],\"words\":[{\"word\":\"concourir\",\"score\":0.7},{\"word\":\"scientifique\",\"score\":0.56},{\"word\":\"science\",\"score\":0.49},{\"word\":\"d\\u00e9veloppement\",\"score\":0.49}],\"name\":\"7\"},{\"value\":172,\"partitions\":[{\"value\":21.0,\"name\":\"sant\\u00e9\"},{\"value\":31.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":40.0,\"name\":\"culture sciences\"},{\"value\":3.0,\"name\":\"amicales groupts affinit\"},{\"value\":10.0,\"name\":\"recherche\"},{\"value\":1.0,\"name\":\"s\\u00e9curit\\u00e9\"},{\"value\":3.0,\"name\":\"clubs loisirs\"},{\"value\":1.0,\"name\":\"clubs\"},{\"value\":22.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":8.0,\"name\":\"environnement\"},{\"value\":1.0,\"name\":\"amicales groupts affinitaires\"},{\"value\":8.0,\"name\":\"culture\"},{\"value\":7.0,\"name\":\"activit\\u00e9s \\u00e9conomiques\"},{\"value\":5.0,\"name\":\"sports\"},{\"value\":1.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":2.0,\"name\":\"tourisme\"},{\"value\":3.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":1.0,\"name\":\"religion\"},{\"value\":1.0,\"name\":\"education formation\"},{\"value\":1.0,\"name\":\"activit\\u00e9s politiques\"},{\"value\":1.0,\"name\":\"caritatives humanit dev\"},{\"value\":1.0,\"name\":\"action socioculturelle\"}],\"words\":[{\"word\":\"d\\u00e9velopper\",\"score\":0.66},{\"word\":\"contribuer\",\"score\":0.65},{\"word\":\"concerner\",\"score\":0.63},{\"word\":\"d\\u00e9veloppement\",\"score\":0.63}],\"name\":\"8\"},{\"value\":118,\"partitions\":[{\"value\":18.0,\"name\":\"action socioculturelle\"},{\"value\":23.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":5.0,\"name\":\"culture\"},{\"value\":8.0,\"name\":\"sports\"},{\"value\":4.0,\"name\":\"aide emploi\"},{\"value\":4.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":4.0,\"name\":\"d\\u00e9fense droits\"},{\"value\":1.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":4.0,\"name\":\"sant\\u00e9\"},{\"value\":3.0,\"name\":\"amicales groupts affinit\"},{\"value\":11.0,\"name\":\"interventions sociales\"},{\"value\":7.0,\"name\":\"caritatives humanit dev\"},{\"value\":5.0,\"name\":\"services medico sociaux\"},{\"value\":3.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":6.0,\"name\":\"environnement\"},{\"value\":1.0,\"name\":\"d\\u00e9fense droits\"},{\"value\":1.0,\"name\":\"religion\"},{\"value\":1.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":1.0,\"name\":\"recherche\"},{\"value\":6.0,\"name\":\"culture sciences\"},{\"value\":2.0,\"name\":\"clubs\"}],\"words\":[{\"word\":\"contribuer\",\"score\":0.69},{\"word\":\"favoriser\",\"score\":0.66},{\"word\":\"d\\u00e9velopper\",\"score\":0.65},{\"word\":\"action\",\"score\":0.64}],\"name\":\"9\"},{\"value\":101,\"partitions\":[{\"value\":24.0,\"name\":\"sant\\u00e9\"},{\"value\":14.0,\"name\":\"interventions sociales\"},{\"value\":22.0,\"name\":\"services medico sociaux\"},{\"value\":4.0,\"name\":\"caritatives humanit dev\"},{\"value\":15.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":1.0,\"name\":\"tourisme\"},{\"value\":4.0,\"name\":\"aide emploi\"},{\"value\":1.0,\"name\":\"activit\\u00e9s \\u00e9conomiques\"},{\"value\":2.0,\"name\":\"justice\"},{\"value\":4.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":2.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":1.0,\"name\":\"sports\"},{\"value\":1.0,\"name\":\"amicales groupts affinit\"},{\"value\":1.0,\"name\":\"s\\u00e9curit\\u00e9\"},{\"value\":4.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":1.0,\"name\":\"recherche\"}],\"words\":[{\"word\":\"concerner\",\"score\":0.64},{\"word\":\"service\",\"score\":0.63},{\"word\":\"action\",\"score\":0.62},{\"word\":\"assurer\",\"score\":0.62}],\"name\":\"10\"},{\"value\":76,\"partitions\":[{\"value\":69.0,\"name\":\"sant\\u00e9\"},{\"value\":1.0,\"name\":\"recherche\"},{\"value\":2.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":1.0,\"name\":\"interventions sociales\"},{\"value\":1.0,\"name\":\"services medico sociaux\"},{\"value\":2.0,\"name\":\"arm\\u00e9e anc combattants\"}],\"words\":[{\"word\":\"m\\u00e9dical\",\"score\":0.67},{\"word\":\"pathologie\",\"score\":0.6},{\"word\":\"prise\",\"score\":0.6},{\"word\":\"patient\",\"score\":0.6}],\"name\":\"11\"},{\"value\":135,\"partitions\":[{\"value\":24.0,\"name\":\"interventions sociales\"},{\"value\":8.0,\"name\":\"sant\\u00e9\"},{\"value\":18.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":11.0,\"name\":\"action socioculturelle\"},{\"value\":35.0,\"name\":\"services medico sociaux\"},{\"value\":13.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":9.0,\"name\":\"caritatives humanit dev\"},{\"value\":4.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":2.0,\"name\":\"d\\u00e9fense droits\"},{\"value\":1.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":3.0,\"name\":\"culture\"},{\"value\":5.0,\"name\":\"amicales groupts affinit\"},{\"value\":1.0,\"name\":\"s\\u00e9curit\\u00e9\"},{\"value\":1.0,\"name\":\"environnement\"}],\"words\":[{\"word\":\"famille\",\"score\":0.66},{\"word\":\"enfant\",\"score\":0.64},{\"word\":\"jeune\",\"score\":0.59},{\"word\":\"tout\",\"score\":0.59}],\"name\":\"12\"},{\"value\":71,\"partitions\":[{\"value\":6.0,\"name\":\"interventions sociales\"},{\"value\":5.0,\"name\":\"environnement\"},{\"value\":5.0,\"name\":\"amicales groupts affinit\"},{\"value\":6.0,\"name\":\"action socioculturelle\"},{\"value\":1.0,\"name\":\"aide emploi\"},{\"value\":5.0,\"name\":\"sant\\u00e9\"},{\"value\":7.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":6.0,\"name\":\"services medico sociaux\"},{\"value\":4.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":2.0,\"name\":\"caritatives humanit dev\"},{\"value\":3.0,\"name\":\"s\\u00e9curit\\u00e9\"},{\"value\":4.0,\"name\":\"culture\"},{\"value\":5.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":2.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":2.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":2.0,\"name\":\"activit\\u00e9s politiques\"},{\"value\":3.0,\"name\":\"sports\"},{\"value\":1.0,\"name\":\"d\\u00e9fense droits\"},{\"value\":1.0,\"name\":\"clubs loisirs\"},{\"value\":1.0,\"name\":\"clubs\"}],\"words\":[{\"word\":\"association\",\"score\":0.73},{\"word\":\"action\",\"score\":0.65},{\"word\":\"regrouper\",\"score\":0.63},{\"word\":\"initiative\",\"score\":0.62}],\"name\":\"13\"},{\"value\":85,\"partitions\":[{\"value\":16.0,\"name\":\"recherche\"},{\"value\":54.0,\"name\":\"culture sciences\"},{\"value\":7.0,\"name\":\"culture\"},{\"value\":2.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":3.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":2.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":1.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"science\",\"score\":0.7},{\"word\":\"scientifique\",\"score\":0.64},{\"word\":\"\\u00e9tude\",\"score\":0.57},{\"word\":\"histoire\",\"score\":0.55}],\"name\":\"14\"},{\"value\":122,\"partitions\":[{\"value\":55.0,\"name\":\"pr\\u00e9servation patrimoine\"},{\"value\":46.0,\"name\":\"culture sciences\"},{\"value\":4.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":9.0,\"name\":\"culture\"},{\"value\":2.0,\"name\":\"environnement\"},{\"value\":1.0,\"name\":\"clubs loisirs\"},{\"value\":5.0,\"name\":\"recherche\"}],\"words\":[{\"word\":\"historique\",\"score\":0.63},{\"word\":\"patrimoine\",\"score\":0.61},{\"word\":\"ancien\",\"score\":0.57},{\"word\":\"monument\",\"score\":0.55}],\"name\":\"15\"},{\"value\":31,\"partitions\":[{\"value\":14.0,\"name\":\"interventions sociales\"},{\"value\":5.0,\"name\":\"sant\\u00e9\"},{\"value\":3.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":2.0,\"name\":\"caritatives humanit dev\"},{\"value\":5.0,\"name\":\"services medico sociaux\"},{\"value\":2.0,\"name\":\"arm\\u00e9e anc combattants\"}],\"words\":[{\"word\":\"aider\",\"score\":0.55},{\"word\":\"d\\u00e9munir\",\"score\":0.52},{\"word\":\"aider\",\"score\":0.52},{\"word\":\"r\\u00e9ins\\u00e9rer\",\"score\":0.52}],\"name\":\"16\"},{\"value\":31,\"partitions\":[{\"value\":16.0,\"name\":\"sant\\u00e9\"},{\"value\":2.0,\"name\":\"arm\\u00e9e anc combattants\"},{\"value\":4.0,\"name\":\"services medico sociaux\"},{\"value\":2.0,\"name\":\"interventions sociales\"},{\"value\":3.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":1.0,\"name\":\"caritatives humanit dev\"},{\"value\":2.0,\"name\":\"amicales groupts affinit\"},{\"value\":1.0,\"name\":\"action socioculturelle\"}],\"words\":[{\"word\":\"malade\",\"score\":0.71},{\"word\":\"malade\",\"score\":0.65},{\"word\":\"soigner\",\"score\":0.6},{\"word\":\"hospitaliser\",\"score\":0.57}],\"name\":\"17\"},{\"value\":20,\"partitions\":[{\"value\":19.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":1.0,\"name\":\"environnement\"}],\"words\":[{\"word\":\"horticulture\",\"score\":0.77},{\"word\":\"horticole\",\"score\":0.73},{\"word\":\"fruitier\",\"score\":0.68},{\"word\":\"floriculture\",\"score\":0.66}],\"name\":\"18\"},{\"value\":9,\"partitions\":[{\"value\":9.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"tuberculose\",\"score\":0.81},{\"word\":\"maladie\",\"score\":0.7},{\"word\":\"infectieux\",\"score\":0.65},{\"word\":\"tuberculeux\",\"score\":0.64}],\"name\":\"19\"},{\"value\":56,\"partitions\":[{\"value\":43.0,\"name\":\"environnement\"},{\"value\":2.0,\"name\":\"sant\\u00e9\"},{\"value\":1.0,\"name\":\"recherche\"},{\"value\":2.0,\"name\":\"s\\u00e9curit\\u00e9\"},{\"value\":4.0,\"name\":\"culture sciences\"},{\"value\":1.0,\"name\":\"amicales groupts affinit\"},{\"value\":1.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":2.0,\"name\":\"sports\"}],\"words\":[{\"word\":\"animal\",\"score\":0.62},{\"word\":\"nature\",\"score\":0.59},{\"word\":\"autre\",\"score\":0.55},{\"word\":\"esp\\u00e8ce\",\"score\":0.55}],\"name\":\"20\"},{\"value\":41,\"partitions\":[{\"value\":4.0,\"name\":\"caritatives humanit dev\"},{\"value\":8.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":2.0,\"name\":\"sant\\u00e9\"},{\"value\":3.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":3.0,\"name\":\"culture\"},{\"value\":7.0,\"name\":\"religion\"},{\"value\":5.0,\"name\":\"interventions sociales\"},{\"value\":1.0,\"name\":\"recherche\"},{\"value\":1.0,\"name\":\"amicales groupts affinit\"},{\"value\":4.0,\"name\":\"services medico sociaux\"},{\"value\":2.0,\"name\":\"action socioculturelle\"},{\"value\":1.0,\"name\":\"arm\\u00e9e anc combattants\"}],\"words\":[{\"word\":\"religieux\",\"score\":0.61},{\"word\":\"catholique\",\"score\":0.57},{\"word\":\"chr\\u00e9tien\",\"score\":0.57},{\"word\":\"spirituel\",\"score\":0.53}],\"name\":\"21\"},{\"value\":43,\"partitions\":[{\"value\":19.0,\"name\":\"services medico sociaux\"},{\"value\":6.0,\"name\":\"sant\\u00e9\"},{\"value\":9.0,\"name\":\"interventions sociales\"},{\"value\":6.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":1.0,\"name\":\"sports\"},{\"value\":1.0,\"name\":\"social\"},{\"value\":1.0,\"name\":\"action socioculturelle\"}],\"words\":[{\"word\":\"enfant\",\"score\":0.68},{\"word\":\"adolescent\",\"score\":0.68},{\"word\":\"adulte\",\"score\":0.66},{\"word\":\"enfance\",\"score\":0.59}],\"name\":\"22\"},{\"value\":13,\"partitions\":[{\"value\":12.0,\"name\":\"int\\u00e9r\\u00eats \\u00e9conomiques\"},{\"value\":1.0,\"name\":\"culture\"}],\"words\":[{\"word\":\"industrie\",\"score\":0.79},{\"word\":\"d\\u00e9veloppement\",\"score\":0.68},{\"word\":\"industriel\",\"score\":0.67},{\"word\":\"secteur\",\"score\":0.65}],\"name\":\"23\"},{\"value\":45,\"partitions\":[{\"value\":44.0,\"name\":\"sant\\u00e9\"},{\"value\":1.0,\"name\":\"caritatives humanit dev\"}],\"words\":[{\"word\":\"tuberculose\",\"score\":0.68},{\"word\":\"maladie\",\"score\":0.64},{\"word\":\"antituberculeux\",\"score\":0.61},{\"word\":\"tuberculeux\",\"score\":0.59}],\"name\":\"24\"},{\"value\":1,\"partitions\":[{\"value\":1.0,\"name\":\"religion\"}],\"words\":[{\"word\":\"diaconie\",\"score\":1.0},{\"word\":\"udv\",\"score\":0.76},{\"word\":\"diaconal\",\"score\":0.68},{\"word\":\"syform\",\"score\":0.52}],\"name\":\"25\"},{\"value\":38,\"partitions\":[{\"value\":31.0,\"name\":\"services medico sociaux\"},{\"value\":5.0,\"name\":\"sant\\u00e9\"},{\"value\":1.0,\"name\":\"aide emploi\"},{\"value\":1.0,\"name\":\"culture\"}],\"words\":[{\"word\":\"handicap\",\"score\":0.59},{\"word\":\"personne\",\"score\":0.57},{\"word\":\"d\\u00e9ficient\",\"score\":0.57},{\"word\":\"sp\\u00e9cifique\",\"score\":0.56}],\"name\":\"26\"},{\"value\":4,\"partitions\":[{\"value\":4.0,\"name\":\"environnement\"}],\"words\":[{\"word\":\"animal\",\"score\":0.75},{\"word\":\"secourir\",\"score\":0.59},{\"word\":\"errant\",\"score\":0.55},{\"word\":\"euthanasi\\u00e9\",\"score\":0.53}],\"name\":\"27\"},{\"value\":6,\"partitions\":[{\"value\":6.0,\"name\":\"environnement\"}],\"words\":[{\"word\":\"animal\",\"score\":0.66},{\"word\":\"prot\\u00e9ger\",\"score\":0.64},{\"word\":\"d\\u00e9fendre\",\"score\":0.52},{\"word\":\"animal\",\"score\":0.48}],\"name\":\"28\"},{\"value\":39,\"partitions\":[{\"value\":9.0,\"name\":\"amicales groupts affinit\"},{\"value\":8.0,\"name\":\"sant\\u00e9\"},{\"value\":13.0,\"name\":\"caritatives humanit dev\"},{\"value\":1.0,\"name\":\"logement\"},{\"value\":4.0,\"name\":\"serv familiaux p \\u00e2g\\u00e9es\"},{\"value\":1.0,\"name\":\"services medico sociaux\"},{\"value\":1.0,\"name\":\"\\u00e9ducation formation\"},{\"value\":1.0,\"name\":\"interventions sociales\"},{\"value\":1.0,\"name\":\"environnement\"}],\"words\":[{\"word\":\"indigent\",\"score\":0.56},{\"word\":\"ville\",\"score\":0.48},{\"word\":\"d\\u00e9partement\",\"score\":0.47},{\"word\":\"commune\",\"score\":0.47}],\"name\":\"29\"},{\"value\":3,\"partitions\":[{\"value\":3.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"transfusion\",\"score\":0.86},{\"word\":\"sanguin\",\"score\":0.84},{\"word\":\"labile\",\"score\":0.78},{\"word\":\"transfusionnel\",\"score\":0.76}],\"name\":\"30\"},{\"value\":4,\"partitions\":[{\"value\":4.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"pathologie\",\"score\":0.73},{\"word\":\"physiopathologique\",\"score\":0.7},{\"word\":\"physiopathologie\",\"score\":0.7},{\"word\":\"pathologique\",\"score\":0.7}],\"name\":\"31\"},{\"value\":7,\"partitions\":[{\"value\":3.0,\"name\":\"caritatives humanit dev\"},{\"value\":1.0,\"name\":\"d\\u00e9fense droits\"},{\"value\":1.0,\"name\":\"culture\"},{\"value\":1.0,\"name\":\"recherche\"},{\"value\":1.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"international\",\"score\":0.59},{\"word\":\"asie\",\"score\":0.58},{\"word\":\"cambodge\",\"score\":0.57},{\"word\":\"pays\",\"score\":0.57}],\"name\":\"32\"},{\"value\":5,\"partitions\":[{\"value\":2.0,\"name\":\"services medico sociaux\"},{\"value\":1.0,\"name\":\"environnement\"},{\"value\":1.0,\"name\":\"amicales groupts affinit\"},{\"value\":1.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"secourir\",\"score\":0.77},{\"word\":\"secourir\",\"score\":0.62},{\"word\":\"orphelin\",\"score\":0.52},{\"word\":\"secours\",\"score\":0.52}],\"name\":\"33\"},{\"value\":1,\"partitions\":[{\"value\":1.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"presqu'\",\"score\":0.63},{\"word\":\"bron\",\"score\":0.53},{\"word\":\"presqu'\",\"score\":0.51},{\"word\":\"\\u00eele\",\"score\":0.48}],\"name\":\"34\"},{\"value\":1,\"partitions\":[{\"value\":1.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"tuberculeux\",\"score\":0.77},{\"word\":\"tuberculeux\",\"score\":0.59},{\"word\":\"tuberculose\",\"score\":0.57},{\"word\":\"bacille\",\"score\":0.55}],\"name\":\"35\"},{\"value\":3,\"partitions\":[{\"value\":3.0,\"name\":\"\\u00e9ducation formation\"}],\"words\":[{\"word\":\"\\u00e9tudiant\",\"score\":0.64},{\"word\":\"professeur\",\"score\":0.61},{\"word\":\"universit\\u00e9\",\"score\":0.57},{\"word\":\"universitaire\",\"score\":0.56}],\"name\":\"36\"},{\"value\":5,\"partitions\":[{\"value\":4.0,\"name\":\"caritatives humanit dev\"},{\"value\":1.0,\"name\":\"amicales groupts affinit\"}],\"words\":[{\"word\":\"bienfaisance\",\"score\":0.87},{\"word\":\"assistance\",\"score\":0.62},{\"word\":\"charitable\",\"score\":0.45},{\"word\":\"caritatif\",\"score\":0.44}],\"name\":\"37\"},{\"value\":1,\"partitions\":[{\"value\":1.0,\"name\":\"amicales groupts affinit\"}],\"words\":[{\"word\":\"outre-mer\",\"score\":0.74},{\"word\":\"outre-mer\",\"score\":0.63},{\"word\":\"d\\u00e9partement\",\"score\":0.63},{\"word\":\"saint-pierre-et-miquelon\",\"score\":0.58}],\"name\":\"38\"},{\"value\":1,\"partitions\":[{\"value\":1.0,\"name\":\"sant\\u00e9\"}],\"words\":[{\"word\":\"isoler\",\"score\":0.5},{\"word\":\"maison\",\"score\":0.47},{\"word\":\"enfant\",\"score\":0.46},{\"word\":\"tout\",\"score\":0.44}],\"name\":\"39\"},{\"value\":2,\"partitions\":[{\"value\":2.0,\"name\":\"services medico sociaux\"}],\"words\":[{\"word\":\"cure\",\"score\":0.62},{\"word\":\"\\u00e9tablissement\",\"score\":0.57},{\"word\":\"activit\\u00e9\",\"score\":0.51},{\"word\":\"sp\\u00e9cifique\",\"score\":0.51}],\"name\":\"40\"}]";
 
  
  clouds: Array<Cloud>;

  @ViewChild('container4Words') element: ElementRef;
  private htmlElement: SVGSVGElement;

  constructor() {
  }

  onClickMe() {
    let data = JSON.parse(this.rawData);    
    let output = squarify<Custom>(data, { x0: 0, y0: 0, x1: this.htmlElement.getBoundingClientRect().width, y1: this.htmlElement.getBoundingClientRect().height });
    let convertion: Array<Cloud> = [];
    output.forEach(element => {
      convertion.push(Cloud.builder().withPartitions(element.partitions).withWords(element.words).withLegend(element.name).from(element).build());
    });
    this.clouds = convertion;    
  }

  ngAfterViewInit(): void {
    this.htmlElement = <SVGSVGElement>this.element.nativeElement;
  }
}
interface Custom {
  name: string;
  words: Array<Word>;
  partitions: Array<Partition>;
}

class Word {
  score: number;
  word: string;
}

class Partition {
  value: number;
  name: string;
}

class Cloud {
  width: number;
  height: number;
  x: number;
  y: number;  
  opacity: number;
  words:Array<Word>;
  partitions:Array<Partition>;
  size: number;
  legend:string = "";

  static builder(): Builder.CloudBuilder {
    return new Builder.CloudBuilder();
  }
}

export module Builder {
  export class CloudBuilder {
    width: number;
    height: number;
    x: number;
    y: number;
    size: number;
    words: Array<Word>;
    partitions:Array<Partition>;
    legend:string;

    getRandomColor(): string {
      let letters = '0123456789ABCDEF';
      let color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    }

    from(rect: ILayoutRect<Custom>): CloudBuilder {
      this.x = rect.x0;
      this.y = rect.y0;
      this.width = rect.x1 - rect.x0;
      this.height = rect.y1 - rect.y0;
      this.size = rect.value;
      return this;
    }

    dimensionByWH(width: number, height: number): CloudBuilder {
      this.width = width;
      this.height = height;
      return this;
    }

    cornerByXY(x: number, y: number): CloudBuilder {
      this.x = x;
      this.y = y;
      return this;
    }

    withLegend(legend:string): CloudBuilder {
      this.legend = legend;
      return this;
    }

    withPartitions(partitions:Array<Partition>): CloudBuilder {
      this.partitions = partitions;
      return this;
    }

    withWords(words:Array<Word>): CloudBuilder {
      this.words = words;
      return this;
    }

    build(): Cloud {
      let cloud = new Cloud();
      cloud.x = this.x;
      cloud.opacity = this.words[0].score;
      cloud.y = this.y;
      cloud.width = this.width;
      cloud.height = this.height;
      cloud.words = this.words;
      cloud.size = this.size;
      cloud.legend = this.legend;
      cloud.partitions = this.partitions;
      return cloud;
    }
  }
}