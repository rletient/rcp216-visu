import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { AppComponent } from './app.component';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { TreeWordCloudDirective } from './word-cloud/tree-word-cloud.directive';
import { RowWordCloudDirective } from './word-cloud/row-word-cloud.directive';
import { PartitionDirective } from './word-cloud/partition.directive';

@NgModule({
  declarations: [
    AppComponent,
    RowWordCloudDirective,
    TreeWordCloudDirective,
    PartitionDirective
  ],
  imports: [
    BrowserModule, MatFormFieldModule, FormsModule,
    MatIconModule, MatInputModule, MatButtonModule, MatCardModule, MatTabsModule, BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
