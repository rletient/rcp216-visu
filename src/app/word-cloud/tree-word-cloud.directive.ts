import { OnInit, Directive, ElementRef, Input, AfterViewInit } from '@angular/core';
import squarify, { IRect, ILayoutRect } from 'squarify';
@Directive({
  selector: 'svg[WordCloudTree]', exportAs: 'word-cloud-tree'
})
export class TreeWordCloudDirective {
  @Input() words: Array<Word>;

  private element: ElementRef;

  public constructor(element: ElementRef) {
    this.element = element;

  }

  ngAfterViewInit() {
    let svgElement = <SVGSVGElement>this.element.nativeElement;
    
    
    let customs:Array<Custom> = [];
    this.words.forEach(word => {
      let custom = new Custom();
      custom.value = word.score;
      custom.word = word;
      customs.push(custom);
    });

    let output = squarify<Custom>(customs, { x0: 0, y0: 0, x1: svgElement.getBoundingClientRect().width, y1: svgElement.getBoundingClientRect().height });
    
    let words:Array<WordGUI> = []
    let first = true;
    let opacityRatioUnit:number;
    output.forEach(element => {
      let to = <ILayoutRect<Custom>>element;      
      if(first) {
        first = false;
        opacityRatioUnit = 1 / to.value;
      }
      
      let word = WordGUI.builder().from(element).withOpacity(opacityRatioUnit * to.value).withConstraintWeight(element.value).withText(to.word.word);
      word.display(svgElement);
      words.push(word);      
    });
  }

  ngOnInit() {

  }
}

class Custom {  
  word: Word;
  value: number;
}

class Word {
  score: number;
  word: string;
}

class WordGUI {
  width: number;
  height: number;
  x: number;
  y: number;
  color: string;
  text: string;
  weight: number;
  opacity:number;

  svg:SVGSVGElement;

  display(element: SVGSVGElement) {

    // CREATION SVG
    this.svg = <SVGSVGElement>document.createElementNS("http://www.w3.org/2000/svg", "svg");
    this.svg.setAttribute("x", `${this.x}px`);
    this.svg.setAttribute("y", `${this.y}px`);
    this.svg.setAttribute("width", `${this.width}px`);
    this.svg.setAttribute("height", `${this.height}px`);
    this.svg.setAttribute("transform", "rotate(315)");

    element.appendChild(this.svg);

    // CREATION TEXT

    let text = <SVGTextElement>document.createElementNS("http://www.w3.org/2000/svg", "text");
    text.setAttribute("x", "10px");
    text.setAttribute("y", "10px");
    text.setAttribute("font-family", "Arial");
    text.setAttribute("font-size", "20");
    text.setAttribute("fill", "white");
    text.setAttribute("opacity", `${this.opacity}`);
    
    text.setAttribute("stroke", "white");
    text.setAttribute("stroke-width", "1");
    this.svg.appendChild(text);

    text.appendChild(document.createTextNode(this.text));
    this.svg.setAttribute("viewBox", `${text.getBBox().x} ${text.getBBox().y} ${text.getBBox().width} ${text.getBBox().height}`);
  }

  getFontHeight():number {
    return this.svg.getBoundingClientRect().height;
  }

  static builder(): Builder.WordGUIBuilder {
    return new Builder.WordGUIBuilder();
  }
}

export module Builder {
  export class WordGUIBuilder {

    width: number;
    height: number;
    x: number;
    y: number;
    color: string;
    text: string;
    weight: number;
    opacity: number;

    withOpacity(opacity: number): WordGUIBuilder {
      this.opacity = opacity;
      return this;
    }

    getRandomColor(): string {
      let letters = '0123456789ABCDEF';
      let color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    }

    withConstraintWeight(weight: number): WordGUIBuilder {
      this.weight = weight;
      return this;
    }

    from(rect: IRect): WordGUIBuilder {
      this.x = rect.x0;
      this.y = rect.y0;
      this.width = rect.x1 - rect.x0;
      this.height = rect.y1 - rect.y0;
      return this;
    }

    dimensionByWH(width: number, height: number): WordGUIBuilder {
      this.width = width;
      this.height = height;
      return this;
    }

    cornerByXY(x: number, y: number): WordGUIBuilder {
      this.x = x;
      this.y = y;
      return this;
    }

    withText(text: string): WordGUI {
      let rectangle = new WordGUI();
      rectangle.text = text;
      rectangle.x = this.x;
      rectangle.opacity = this.opacity;
      rectangle.color = this.getRandomColor();
      rectangle.y = this.y;
      rectangle.width = this.width;
      rectangle.height = this.height;
      return rectangle;
    }
  }
}