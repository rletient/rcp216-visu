import { OnInit, Directive, ElementRef, Input, AfterViewInit } from '@angular/core';
import squarify, { IRect, ILayoutRect } from 'squarify';
@Directive({
  selector: 'svg[WordCloudRow]', exportAs: 'word-cloud-row'
})
export class RowWordCloudDirective {

   
  @Input() words: Array<Word>;

  private element: ElementRef;

  public constructor(element: ElementRef) {
    this.element = element;
  }

  ngAfterViewInit() {
    let svgElement = <SVGSVGElement>this.element.nativeElement;    
    let maxLevel = 0;
    
    this.words = this.words.sort((n1, n2) => n2.score - n1.score);
    
    this.words.forEach(data => {
      maxLevel = Math.max(maxLevel, data.score);
    });

    let opacityRatio = 1 / maxLevel;
    let heightItem = (svgElement.getBoundingClientRect().height -15) / this.words.length;
    let currentY = 15;
    this.words.forEach(word => {
      WordGUI.builder().cornerByXY(0, currentY).dimensionByWH(svgElement.getBoundingClientRect().width, heightItem)
        .withOpacity(opacityRatio * word.score)
        .withTooltip(`Pertinence : ${word.score}`)
        .withText(word.word).display(svgElement);
        currentY += heightItem;
    });
  }

  ngOnInit() {

  }
}
class Word {
  score: number;
  word: string;
}

class WordGUI {
  width: number;
  height: number;
  x: number;
  y: number;  
  text: string;
  tooltip: string;
  weight: number;
  opacity: number;

  svg: SVGSVGElement;

  display(element: SVGSVGElement) {

    // CREATION SVG
    this.svg = <SVGSVGElement>document.createElementNS("http://www.w3.org/2000/svg", "svg");
    this.svg.setAttribute("x", `${this.x}px`);
    this.svg.setAttribute("y", `${this.y}px`);
    this.svg.setAttribute("width", `${this.width}px`);
    this.svg.setAttribute("height", `${this.height}px`);
    

    element.appendChild(this.svg);

    // CREATION TEXT

    let text = <SVGTextElement>document.createElementNS("http://www.w3.org/2000/svg", "text");
    text.setAttribute("x", "10px");
    text.setAttribute("y", "10px");
    text.setAttribute("font-family", "Arial");
    text.setAttribute("font-size", "20");
    text.setAttribute("fill", "white");
    text.setAttribute("opacity", `${this.opacity}`);

    text.setAttribute("stroke", "white");
    text.setAttribute("stroke-width", "1");
    this.svg.appendChild(text);

    let title = <SVGTextElement>document.createElementNS("http://www.w3.org/2000/svg", "title");
    title.appendChild(document.createTextNode(this.tooltip));

    text.appendChild(title);

    text.appendChild(document.createTextNode(this.text));
    this.svg.setAttribute("viewBox", `${text.getBBox().x} ${text.getBBox().y} ${text.getBBox().width} ${text.getBBox().height}`);
  }

  getFontHeight(): number {
    return this.svg.getBoundingClientRect().height;
  }

  static builder(): Builder.WordGUIBuilder {
    return new Builder.WordGUIBuilder();
  }
}

export module Builder {
  export class WordGUIBuilder {

    width: number;
    height: number;
    x: number;
    y: number;    
    text: string;
    tooltip: string;
    weight: number;
    opacity: number;

    withOpacity(opacity: number): WordGUIBuilder {
      this.opacity = opacity;
      return this;
    }

    dimensionByWH(width: number, height: number): WordGUIBuilder {
      this.width = width;
      this.height = height;
      return this;
    }

    cornerByXY(x: number, y: number): WordGUIBuilder {
      this.x = x;
      this.y = y;
      return this;
    }

    withTooltip(text: string): WordGUIBuilder {
      this.tooltip = text;
      return this;
    }

    withText(text: string): WordGUI {
      let rectangle = new WordGUI();
      rectangle.text = text;
      rectangle.x = this.x;
      rectangle.opacity = this.opacity;
      rectangle.tooltip = this.tooltip;
      rectangle.y = this.y;
      rectangle.width = this.width;
      rectangle.height = this.height;
      return rectangle;
    }
  }
}