import { OnInit, Directive, ElementRef, Input, AfterViewInit } from '@angular/core';
import squarify, { IRect, ILayoutRect } from 'squarify';

@Directive({
  selector: 'svg[Partition]', exportAs: 'partition'
})
export class PartitionDirective implements AfterViewInit {


  @Input() partitions: Array<Partition>;
  
  private element: ElementRef;

  public constructor(element: ElementRef) {
    this.element = element;
  }

  ngAfterViewInit(): void {
    let svgElement = <SVGSVGElement>this.element.nativeElement;

    let customs: Array<Custom> = [];
    this.partitions.forEach(partition => {
      let custom = new Custom();
      custom.value = partition.value;
      custom.partition = partition;
      customs.push(custom);
    });
    let margin = 3;
    let output = squarify<Custom>(customs, { x0: 0 + margin, y0: 0 + margin, x1: svgElement.getBoundingClientRect().width - margin, y1: svgElement.getBoundingClientRect().height - margin });
    output.forEach(element => {
      let layoutRect = <ILayoutRect<Custom>>element;
      let rectangle = Rectangle.builder().from(layoutRect).withText(layoutRect.partition.name).withTooltip(`${layoutRect.partition.name} - ${layoutRect.partition.value}`).build();
      rectangle.display(svgElement);
    });
  }

}

class Partition {
  value: number;
  name: string;
}

class Custom {
  partition: Partition;
  value: number;
}
class Rectangle {
  width: number;
  height: number;
  x: number;
  y: number;
  opacity: number;
  tooltip: string;
  text:string = "TOTO";

  display(element: SVGSVGElement) {
    if (this.width < 0 || this.height < 0) {
      return;
    }
    let margin = 0;
    // CREATION SVG
    let rect = <SVGRectElement>document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("x", `${this.x}px`);
    rect.setAttribute("y", `${this.y}px`);
    rect.setAttribute("width", `${this.width}px`);
    rect.setAttribute("height", `${this.height}px`);

    rect.setAttribute("stroke", "white");
    rect.setAttribute("stroke-width", "1");
    rect.setAttribute("stroke-dasharray", "5,5");

    let title = <SVGRectElement>document.createElementNS("http://www.w3.org/2000/svg", "title");

    title.appendChild(document.createTextNode(this.tooltip));
    rect.appendChild(title);
    element.appendChild(rect);

    // -----------------
    // CREATION SVG
    let svg = <SVGSVGElement>document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("x", `${this.x}px`);
    svg.setAttribute("y", `${this.y}px`);
    svg.setAttribute("width", `${this.width}px`);
    svg.setAttribute("height", `${this.height}px`);
    svg.setAttribute("transform", "rotate(315)");

    element.appendChild(svg);

    // CREATION TEXT

    let text = <SVGTextElement>document.createElementNS("http://www.w3.org/2000/svg", "text");
    text.setAttribute("x", "10px");
    text.setAttribute("y", "10px");
    text.setAttribute("font-family", "Arial");
    text.setAttribute("font-size", "20");
    text.setAttribute("fill", "white");        
    text.setAttribute("stroke", "white");
    text.setAttribute("stroke-width", "1");

    svg.appendChild(text);
    let space = 0;
    this.text.split(" ", 3).forEach(element => {
      
      let tspan = <SVGTextElement>document.createElementNS("http://www.w3.org/2000/svg", "tspan");
      tspan.setAttribute("text-anchor","middle");
      tspan.setAttribute("x","0px");
      tspan.setAttribute("y",`${space}px`);
      space += 15;
      tspan.appendChild(document.createTextNode(element));
      text.appendChild(tspan);
    }); 
    
    svg.setAttribute("viewBox", `${text.getBBox().x} ${text.getBBox().y} ${text.getBBox().width} ${text.getBBox().height}`);
  }

  getRandomColor(): string {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  static builder(): Builder.RectangleBuilder {
    return new Builder.RectangleBuilder();
  }
}

export module Builder {
  export class RectangleBuilder {

    width: number;
    height: number;
    x: number;
    y: number;
    size: number;
    partitions: Array<Partition>;
    tooltip: string;
    text: string;

    withText(text: string): RectangleBuilder {
      this.text = text;
      return this;
    }

    withTooltip(tooltip: string): RectangleBuilder {
      this.tooltip = tooltip;
      return this;
    }

    from(rect: ILayoutRect<Custom>): RectangleBuilder {
      this.x = rect.x0;
      this.y = rect.y0;
      this.width = rect.x1 - rect.x0;
      this.height = rect.y1 - rect.y0;
      this.size = rect.value;
      return this;
    }

    build(): Rectangle {
      let cloud = new Rectangle();
      cloud.x = this.x;
      cloud.y = this.y;
      cloud.width = this.width;
      cloud.height = this.height;
      cloud.tooltip = this.tooltip;
      cloud.text = this.text;
      return cloud;
    }
  }
}